from __future__ import print_function
import os, freesound, time, sys
from tqdm import tqdm

# with open('first_line.csv') as f:
#     soundnames = list(map(lambda x: x.split(',')[1].strip(), f.readlines()))

soundnames = ['slap', 'thud', 'slam']
soundnames = ['beating', 'beeping', 'bleep', 'clapping', 'click', 'ding', 'pop']
soundnames = ['squeaking']
soundnames = ['scratching']
# soundnames = ['thunder']


client = freesound.FreesoundClient()
client.set_token("p4qH5zAHwYgGBSs9IWvxMnslarQ7WGBUmyoSpzkW","token")

for soundname in soundnames:
    print(soundname)
    print('at index {}'.format(soundnames.index(soundname)))
    # if os.path.exists(os.path.join('soundfiles', soundname)):
    #     os.rmdir(os.path.join('soundfiles', soundname))
    if soundname != 'ringing':
        os.mkdir(os.path.join('soundfiles', soundname))
    results = client.text_search(query="{} -reverse".format(soundname),
    fields="id,name,previews,analysis",
    filter="(tag:{0} OR original_filename:{0})".format(soundname))
    print('{} soundfiles'.format(results.count))
    pager = results.count/15
    print('should be {} pages'.format((pager)))
    t1 = time.clock()
    print('time is {}'.format(t1))
    for i in tqdm(range(pager)):
        if i%5==0:
            # print(i)
            time.sleep(1)
        if soundname == 'ringing' and i<30:
            pass
        else:
            for sound in results:
                here_sound_name = sound.name.replace('/', '_')
                try:
                    sound.retrieve_preview(os.path.join('soundfiles', soundname), here_sound_name+".mp3")
                except:
                    print(sys.exc_info())
                    with open('log.log', 'a') as f:
                        f.write("{}\n".format(sound.id))
        if i+1<pager:
            results = results.next_page()
        # time.sleep(1)
    print('end')
